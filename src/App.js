import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Tableaus from './components/Tab.jsx'
import { Tab, Grid, Menu, Container, Header, Segment } from 'semantic-ui-react'

class App extends Component {
        constructor() {
                super()
        }
        state = { activeItem: 'bio' }
        handleItemClick = (e, { name }) => this.setState({ activeItem: name })
        render() {
                const { activeItem } = this.state
                return (
                        <div className="App">
                                <header className="App-header">
                                        <img src={logo} className="App-logo" alt="logo" />
                                </header>
                                <Header as='h1'>Welcome to TradingBot</Header>
                                <Grid>
                                        <Grid.Column width={1}>
                                                <Menu pointing vertical>
                                                        <Menu.Item name='home' active={activeItem === 'home'} onClick={this.handleItemClick} />
                                                        <Menu.Item name='messages' active={activeItem === 'messages'} onClick={this.handleItemClick} />
                                                        <Menu.Item name='friends' active={activeItem === 'friends'} onClick={this.handleItemClick} />
                                                </Menu>
                                        </Grid.Column>
                                </Grid>
                                <Tableaus />
                        </div>
                )
        }
}

export default App;

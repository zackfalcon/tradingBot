import React, { Component } from "react";
import { Line, Bar } from 'react-chartjs-2';
import axios from 'axios';

class Chart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			chartData: {},
			label: props.name
		}
	}
	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'right',
		location: 'City'
	}
	componentWillMount() {
		this.getChartData();
	}
	async componentWillReceiveProps(nextprops) {
		await this.setState({
			label: nextprops.name
		})
		this.getChartData();
	}
	async getChartData() {
		try {
			var response = await axios.get('http://tradeapi/' + this.state.label);
			var labels = [];
			var data = [];
			var color = [];
			response.data.forEach(element => {
				labels.push(element.marketplace);
				data.push(element.value);
				//color.push("rgba(255, " + parseInt(element.value % 255) + ", 132, 0.6)");
			});
			this.setState({
				chartData: {
					labels: labels,
					datasets: [{
						label: this.props.name,
						data: data,
						backgroundColor: color
					}]
				}
			})
		} catch (e) {
			console.log(e);
		}
	}
	render() {
		return (
			<div className="chart">
				<Line
					data={this.state.chartData}
					options={{
						title: {
							display: this.props.displayTitle,
							text: this.state.label,
							fontSize: 25
						},
						legend: {
							display: this.props.displayLegend,
							position: this.props.legendPosition
						},
					}} />
			</div>
		)
	}
}

export default Chart;
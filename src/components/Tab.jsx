import React, { Component } from 'react';
import Chart from './Graph.jsx';
import { Tab, Grid, Menu, Container } from 'semantic-ui-react'

const panes = [
        { menuItem: 'Bitcoin', render: () => <Tab.Pane><Chart name="bitcoin" legendPosition="bottom" /></Tab.Pane> },
        { menuItem: 'Aluminium', render: () => <Tab.Pane><Chart name="aluminium" legendPosition="bottom" /></Tab.Pane> },
]

class Tableau extends Component {
        render() {
                return (
                        <Container>
                                <Tab panes={panes} />
                        </Container>
                )
        }
}

export default Tableau;
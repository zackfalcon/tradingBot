import fetch from 'isomorphic-fetch';

export const GET = (endPoint, foo) =>
  fetch('http://tradeapi' + endPoint, {
    method: 'GET',
  }).then((response) => {
    if (response.status !== 200) {
      return response.json()
        .then(json => Promise.reject({ reason: json, status: response.status }));
    }
    return response.json().then(json => ({ json, headers: response.headers }));
  }).catch((ex) => {
    console.log('ex GET', ex);
    return Promise.reject(ex);
  });
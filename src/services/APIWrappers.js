import { GET } from './APIMethods';

const methodGenerator = (method, url) => (data, queryUrl, urlVars) => {
        let validUrl = `${url}`;
      
        if (urlVars) {
          Object.keys(urlVars).forEach((key) => {
            if (url.indexOf(`:${key}`) === -1) {
              console.log('Invalid url key', url, key);
              return;
            }
            validUrl = validUrl.replace(`:${key}`, urlVars[key]);
          });
        }
        if (queryUrl) {
          validUrl = `${validUrl}${queryUrl}`;
        }
        return method(validUrl, data);
};

export const getCircuits = methodGenerator(GET, '');
